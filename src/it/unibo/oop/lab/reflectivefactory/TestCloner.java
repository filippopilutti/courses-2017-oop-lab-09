package it.unibo.oop.lab.reflectivefactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

/**
 * Used to test reflection on ClonableClass.
 * 
 */
public class TestCloner {

    /**
     * Used to test reflection on a ClonableClass instance.
     * @throws CloningException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * 
     */
	@Test
    public void testInstanceCloning() throws NoSuchMethodException, SecurityException, 
    											InstantiationException, IllegalAccessException, 
    												IllegalArgumentException, InvocationTargetException {
        /*
         * 1) Create an instance of ClonableClass
         */
    	ClonableClass cl = new ClonableClass();
        /*
         * 2) Invoke the setters to configure the object
         */
    	try {
    		cl.setA("Filippo");
    		cl.setB("Ciao");
    		cl.setD(203.0);
    	/*
    	 * 3) Clone the object leveraging ObjectClonerUtil.cloneObj
         */
    		final ClonableClass clone = ObjectClonerUtil.cloneObj(cl, ClonableClass.class);
        /*
         * 4) Use the getters to test the equivalence of the two objects
         */
    	
    		assertEquals("A differs!", cl.getA(), clone.getA());
    		assertEquals("B differs!", cl.getB(), clone.getB());
    		assertEquals("C differs!", cl.getD(), clone.getD());
    	} catch (final CloningException e) {
    		fail();
    	}
    }
}
